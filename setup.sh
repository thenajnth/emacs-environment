#!/bin/bash

# Directory of current file
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Init will throw error when custom.el is missing
mkdir $DIR/.gen
touch $DIR/.gen/custom.el

