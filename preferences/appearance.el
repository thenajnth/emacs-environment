(provide 'appearance)

(scroll-bar-mode -1)

(toggle-tool-bar-mode-from-frame -1)  ;; Removes toolbar
(setq inhibit-startup-screen t)  ;; Removes startup screen

(show-paren-mode 1)

(setq initial-scratch-message nil)

(add-to-list 'default-frame-alist '(font . "Source Code Pro-10"))

(set-face-attribute 'variable-pitch nil :family "IBM Plex Serif")

(setq frame-title-format '("Emacs @ " system-name ": %b %+%+ %f"))

(load-theme 'solarized-dark t)

;; Uncomment to have variable pitch instead of mono fonts in text modes
; (add-hook 'text-mode-hook
;           (lambda ()
;	     (variable-pitch-mode 1)))

;;; appearance ends here
