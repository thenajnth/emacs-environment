(provide 'misc)

;; Use Force Backup (i.e. don't save backups to same folder)
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups

;; Change path to custom.el-file
(setq custom-file "~/.emacs.d/.gen/custom.el")

;; Ignore extensions in completion
(setq completion-ignored-extensions
      '(".o" "~" ".bin" ".dvi" ".toc" ".aux" ".bbl" ".glo" ".pyc"))

;; auto close bracket insertion. New in emacs 24
(electric-pair-mode 1)

;; Do not use tabs for indentation
(setq-default indent-tabs-mode nil)

;; Visual line mode for some modes
(dolist (hook '(LaTeX-mode-hook
		org-mode-hook
		markdown-mode-hook))
  (add-hook hook (lambda () (visual-line-mode t))))

;; Week start on monday
(setq calendar-week-start-day 1)

;; Fix so Emacs client frame get focus on startup
(defun px-raise-frame-and-give-focus ()
  (when window-system
    (raise-frame)
    (x-focus-frame (selected-frame))
    (set-mouse-pixel-position (selected-frame) 4 4)
    ))
(add-hook 'server-switch-hook 'px-raise-frame-and-give-focus)

;; Stop creation of lockfiles
(setq create-lockfiles nil)
