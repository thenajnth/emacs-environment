;;; package --- init-lsp-mode

(provide 'init-lsp-mode)

;; Java
(require 'lsp-java)
(add-hook 'java-mode-hook #'lsp)

;; Python
(require 'lsp-pyright)
(add-hook 'python-mode-hook #'lsp)

;; C#
(require 'lsp-csharp)
(add-hook 'csharp-mode-hook #'lsp)


;;; init-lsp-mode.el ends here
