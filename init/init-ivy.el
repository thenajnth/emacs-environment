;;; package --- init-ivy

(provide 'init-ivy)

(ivy-mode)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)

;;; init-ivy.el ends here
