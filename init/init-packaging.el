;;; package: --- Setting up packaging
;;; Commentary: Install list of packages if they are not already installed.

;;; Code:

(provide 'init-packaging)

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

; (package-initialize)

(defvar my-packages
  '(add-node-modules-path
    all-the-icons  ;; Used by doom-modeline
    auctex
    csharp-mode
    company
    counsel
    doom-modeline
    editorconfig
    flycheck
    ivy
    lsp-mode
    lsp-java
    lsp-pyright
    lsp-ui
    lsp-ivy
    magit
    org-bullets
    projectile
    solarized-theme
    undo-tree
    yasnippet
    yasnippet-snippets
  )
  "A list of packages to ensure are installed at launch.")

(defun my-packages-installed-p ()
  (cl-loop for p in my-packages
           when (not (package-installed-p p)) do (cl-return nil)
           finally (cl-return t)))

(unless (my-packages-installed-p)
  ;; check for new packages (package versions)
  (package-refresh-contents)
  ;; install the missing packages
  (dolist (p my-packages)
    (when (not (package-installed-p p))
      (package-install p))))

;;; init-packaging.el ends here
