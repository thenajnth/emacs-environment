;;; package --- init-projectile

(provide 'init-projectile)


(setq projectile-completion-system 'ivy)

(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;;; init-projectile.el ends here
