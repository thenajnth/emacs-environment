;;; package: --- init-auctex
;;; Commentary: Setting up auxtex

(provide 'init-auctex)
	
(setq TeX-auto-save t)
(setq TeX-parse-self t)
