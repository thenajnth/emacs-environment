;;; package --- init-yasnippet

(provide 'init-yasnippet)

(require 'yasnippet)
(yas-global-mode 1)

;;; init-yasnippet.el ends here
