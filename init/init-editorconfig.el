;;; package: init-editorconfig

;;; Commentary: Getting

(provide 'init-editorconfig)

(editorconfig-mode 1)

;;; init-editorconfig.el ends here

