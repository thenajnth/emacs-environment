;;; package --- init-csharp-mode

(provide 'init-csharp-mode)

(add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-tree-sitter-mode))
;;; init-csharp-mode.el ends here
