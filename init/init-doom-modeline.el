(provide 'init-doom-modeline)

(require 'doom-modeline)

(setq doom-modeline-buffer-file-name-style 'truncate-upto-project)
(setq doom-modeline-lsp t)
(setq doom-modeline-env-version t)

(doom-modeline-mode 1)
