(provide 'init-orgmode)

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
(setq org-startup-indented t)

;; Hide emphasis markers.
(setq org-hide-emphasis-markers t)

;; Header bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; This prevents org-mode odt convert to write an extra flymake_content.xml
;; file to the odt-file which will make it corrupt.
(defadvice org-export-as-odt (around remove-flymake-hook first act)
  (let ((find-file-hook find-file-hook))
    (remove-hook 'find-file-hook 'flymake-find-file-hook)
    ad-do-it))

(require 'ox-odt)

;; Make xdg-open take care of opening the right program of exported odt file (and others)
(setcdr (assq 'system org-file-apps-gnu) "xdg-open %s")


;; Turn on auto fill
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;; Default ODF style
(setq org-export-odt-styles-file "~/.emacs.d/org-export-templates/predikan-style.xml")
