;;; package --- init-dired

(provide 'init-dired)

;; Integrate xdg-open with dired
(defun xdg-open-file (filename)
  "xdg-opens the specified file."
  (interactive "fFile to open: ")
  (let ((process-connection-type nil))
    (start-process "" nil "/usr/bin/xdg-open" filename)))

(defun dired-xdg-open-file ()
  "Opens the current file in a Dired buffer."
  (interactive)
  (xdg-open-file (dired-get-file-for-visit)))


(if (eq system-type 'gnu/linux)
    (add-hook 'dired-mode-hook ; Bound to key E
	      (lambda () (local-set-key "E" 'dired-xdg-open-file)))
)

;; Integrate Windows file open with dired
(defun win-open-file (filename)
  "Windows opens the specified file."
  (interactive "fFile to open: ")
  (let ((process-connection-type nil))
    (w32-shell-execute "open" filename)))

(defun dired-win-open-file ()
  "Opens the current file in a Dired buffer."
  (interactive)
  (win-open-file (dired-get-file-for-visit)))

(if (eq system-type 'windows-nt)
    (add-hook 'dired-mode-hook ; Bound to key E
	      (lambda () (local-set-key "E" 'dired-win-open-file)))
)

;;; init-dired.el ends here
