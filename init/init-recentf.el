(provide 'init-recentf)

;; Enable recent files
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(setq recentf-save-file (expand-file-name "recentf" "~/.emacs.d/.gen/"))
