(add-to-list 'load-path (expand-file-name "~/.emacs.d/init"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/preferences"))

(setq package-enable-at-startup nil) (package-initialize)
(require 'cl-lib)

(mapc 'require '(init-packaging
		 appearance
                 init-all-the-icons  ;; Used by doom-modeline
		 init-auctex
		 ;; init-auto-virtualenv
		 init-dired
		 init-doom-modeline
		 init-editorconfig
		 init-flycheck
		 ;; init-flyspell
		 init-ivy
                 init-lsp-mode
		 init-orgmode
		 init-projectile
                 init-recentf
		 init-undo-tree
		 ;; init-web-mode
		 ;; init-yaml-mode
		 init-yasnippet
		 misc
		 keybinds
		 skeletons
		 ))

(load custom-file)
(put 'dired-find-alternate-file 'disabled nil)
